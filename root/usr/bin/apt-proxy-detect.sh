#!/bin/bash
## Tells  Proxy-Auto-Config if a specified proxy is reachable or if
## it should just fallback to a direct connection.
##
## This file should be executable and referenced by its full path
## in the /etc/apt/apt.conf.d/02proxy file we create too.

## This will install netcat automatically if it's missing if uncommented
#if [[ $(which nc >/dev/null; echo $?) -ne 0 ]]; then
#	apt install -y netcat
#fi

CONF_FILE="/etc/apt-proxy-detect.conf"

if [ -f "$CONF_FILE" ]; then
    . $CONF_FILE
else
    echo "no config file found, not using proxy" 1>&2
    echo -n "DIRECT"
    exit 0
fi

if [ -z "$PROXY" ]; then
    echo "'PROXY' is empty" 1>&2
    echo -n "DIRECT"
elif [ -z "$(which curl)" ]; then
    echo "'curl' not found for connectivity check, not using proxy" 1>&2
    echo -n "DIRECT"
elif [[ $(curl $PROXY &>/dev/null; echo $?) -eq 0 ]]; then
    echo "using proxy $PROXY" 1>&2
    echo -n "$PROXY"
else
    echo "using no proxy, no connection to $PROXY" 1>&2
    echo -n "DIRECT"
fi