#!/bin/bash
#install to /var/lib/nspawn-helper
#overwrites existing files, but does not remove additionals

TGT_DIR="/var/lib/nspawn-helper"
LINKS="create-machine"

#check sudo
if [ $(id -u) -ne 0 ]; then
	echo must be run as root
	exit 1
fi

echo "installing into $TGT_DIR"
if [ -d "$TGT_DIR" ]; then
	echo "target '$TGT_DIR' already exists"
else
	mkdir "$TGT_DIR"
fi
git ls-files | grep -v install.sh | cpio -R root:root -upvd "$TGT_DIR"

#add symlinks to executables into /usr/local/sbin
for link in $LINKS
do
	echo -n "linking "
	ln -svf $TGT_DIR/$link /usr/local/sbin/$link
done
